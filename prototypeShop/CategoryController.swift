//
//  CategoryController.swift
//  prototypeShop
//
//  Created by Ivan Melkozerov on 03.03.2018.
//  Copyright © 2018 Ivan Melkozerov. All rights reserved.
//

import Foundation
import UIKit;

class CategoryController: UITableViewController {
    
    @IBOutlet var categoryTableView: UITableView!;
    
    let reuseCell = "categoryCell";
    let reuseCellSelected = "categoryCellSelected";
    
    let apiUrl = "http://local.baseshop.ru/api/";
    
    var apiCategories: Array<Category> = [];
    
    let defaultCategory: Category = Category(id: 0, name: "Показать все");
    
    var selectedCategory: Category?;
    var categoryChanged = false;
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        self.categoryTableView.delegate = self;
        self.categoryTableView.dataSource = self;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getCategories();
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.apiCategories.count + 1;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let myCell:CategoryTableCell;
        myCell = self.categoryTableView.dequeueReusableCell(withIdentifier: self.reuseCell) as! CategoryTableCell;
        if indexPath.row+1 <= self.apiCategories.count {
            myCell.categoryName.text = self.apiCategories[indexPath.row].name;
        } else {
            myCell.categoryName.text = self.defaultCategory.name;
        }
        return myCell;
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row+1 <= self.apiCategories.count {
            if let selfCat = self.selectedCategory {
                if selfCat.id != self.apiCategories[indexPath.row].id {
                    self.categoryChanged = true;
                }
            } else {
                self.categoryChanged = true;
            }
            self.selectedCategory = self.apiCategories[indexPath.row];
        } else {
            if self.selectedCategory != nil {
                self.categoryChanged = true;
            }
            self.selectedCategory = nil;
        }
        performSegue(withIdentifier: "fromCategoriesToMain", sender: self);
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        print("deselected");
    }
    
    func getCategories() {
        let url = URL(string: self.apiUrl + "getcategories");
        let task = URLSession.shared.dataTask(with: url!) {
            (data, response, error) in
            if let jsonObject = data {
                let decoder = JSONDecoder();
                self.apiCategories = try! decoder.decode([Category].self, from: jsonObject);
                DispatchQueue.main.async {
                    if let tView = self.categoryTableView {
                        tView.reloadData();
                    }
                }
            }
        }
        task.resume();
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let showMainView = segue.destination as? ViewController {
            showMainView.apiItems = [];
            showMainView.offset = 0;
            if let selCat = self.selectedCategory {
                showMainView.selectedCategory = selCat;
            } else {
                showMainView.selectedCategory = nil;
            }
            showMainView.needUpdateList = self.categoryChanged;
            self.categoryChanged = false;
        }
    }
    
    @IBAction func cancelFilterButtonTouched(_ sender: UIButton) {
        self.selectedCategory = nil;
        performSegue(withIdentifier: "fromCategoriesToMain", sender: self);
        
    }
    
}

class CategoryTableCell: UITableViewCell {
    @IBOutlet weak var categoryName: UILabel!
}

struct Category: Decodable {
    var id: Int;
    var name: String;
}
