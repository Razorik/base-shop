//
//  ItemController.swift
//  prototypeShop
//
//  Created by Ivan Melkozerov on 03.03.2018.
//  Copyright © 2018 Ivan Melkozerov. All rights reserved.
//

import Foundation
import UIKit;

class ItemController: UIViewController {
    
    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var nameProduct: UILabel!
    @IBOutlet weak var descriptionProduct: UILabel!
    @IBOutlet weak var priceProduct: UILabel!
    
    var product: Product!;
    
    override func viewWillAppear(_ animated: Bool) {
        if let prod = self.product {
            self.nameProduct.text = prod.name;
            self.descriptionProduct.text = prod.description;
            self.priceProduct.text = String(prod.price) + " руб.";
            if let url = URL(string: prod.image) {
                do {
                    let data = try Data(contentsOf: url);
                    self.imageProduct.image = UIImage(data: data);
                } catch {
                    print("eror");
                }
            }
        }
    }
    
}
