//
//  ViewController.swift
//  prototypeShop
//
//  Created by Ivan Melkozerov on 20.02.2018.
//  Copyright © 2018 Ivan Melkozerov. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let reuseCellName = "shopItem";
    
    var offset = 0;
    
    var take = 10;
    
    let apiUrl = "http://local.baseshop.ru/api/";
    
    var selectedItem: Product!;
    var needUpdateList: Bool = false;
    
    var selectedCategory: Category?;
    
    var apiItems: Array<Product> = [];
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let tView = self.tableView {
            tView.delegate = self;
            tView.dataSource = self;
        }
        
        self.getProducts(take: self.take, offset: self.offset, category: self.selectedCategory);
    }
    
    func refreshProductsList() {
        DispatchQueue.main.async {
            if let tView = self.tableView {
                tView.reloadData();
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        if self.needUpdateList {
            self.getProducts(take: self.take, offset: self.offset, category: self.selectedCategory);
            self.needUpdateList = false;
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return apiItems.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let myCell:MyCustomCell = self.tableView.dequeueReusableCell(withIdentifier: reuseCellName) as! MyCustomCell;
        myCell.myCellLabel.text = self.apiItems[indexPath.row].name;
        myCell.myPriceLabel.text = String(self.apiItems[indexPath.row].price) + " руб.";
        if let url = URL(string: self.apiItems[indexPath.row].image) {
            do {
                let data = try Data(contentsOf: url);
                myCell.myImage.image = UIImage(data: data);
            } catch {
                print("ошибка");
            }
            
        }
        return myCell;
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        print("Вы нажали на строку \(indexPath.row)");
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        self.selectedItem = self.apiItems[indexPath.row];
        return indexPath;
    }
    
    func getProducts(take: Int, offset: Int, category: Category!) {
        var url: URL!;
        if category != nil {
            url = URL(string: self.apiUrl + "getproducts?take=\(take)&offset=\(offset)&category=\(category.id)")!;
        } else {
            url = URL(string: self.apiUrl + "getproducts?take=" + String(take) + "&offset=" + String(offset))!;
        }
        self.requestProducts(url: url);
    }
    
    func requestProducts(url: URL?) {
        let task = URLSession.shared.dataTask(with: url!) {
            (data, response, error) in
            if let jsonObject = data {
                let decoder = JSONDecoder();
                self.apiItems += try! decoder.decode([Product].self, from: jsonObject);
                self.offset = self.apiItems.count;
                self.refreshProductsList();
            }
        }
        task.resume();
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let showItemController = segue.destination as? ItemController {
            if let selItem = self.selectedItem {
                showItemController.product = selItem;
            }
        }
        else if let showCategoryController = segue.destination as? CategoryController {
            if let selCat = self.selectedCategory {
                showCategoryController.selectedCategory = selCat;
            }
        }
    }
    
    @IBAction func goBackGromCategories(segue: UIStoryboardSegue) {
        
    }

}

class MyCustomCell: UITableViewCell {
    
    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var myCellLabel: UILabel!
    @IBOutlet weak var myPriceLabel: UILabel!
    @IBOutlet weak var myImage: UIImageView!
    
}

struct Product: Decodable {
    var id: Int;
    var parent_id: Int;
    var name: String;
    var description:String;
    var price: Int;
    var image: String;
}


